package behruz.project.behruz.controller.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
public class StudentRequestDto {

    @Min(5)
    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;
}
