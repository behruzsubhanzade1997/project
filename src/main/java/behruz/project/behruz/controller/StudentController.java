package behruz.project.behruz.controller;

import behruz.project.behruz.controller.dto.StudentDto;
import behruz.project.behruz.controller.dto.StudentRequestDto;
import behruz.project.behruz.domain.Student;
import behruz.project.behruz.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
@Slf4j
@RestController
//@RequiredArgsConstructor
@RequestMapping("students")
public class StudentController {

//    @Autowired
//    @Qualifier("service")
    private final StudentService service;
    public StudentController(@Qualifier("service1") StudentService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Student getStudent(@PathVariable long id) {
        return service.getById(id);

    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public StudentDto createStudent(@Valid @RequestBody StudentRequestDto student) {
        log.trace("Recevied request {}", student);
        return service.createStudent(student);
    }

    @PutMapping("/{id}")
    public Student updateStudent(@PathVariable Long id, @RequestBody Student student) {
       return service.updateStudent(id, student);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStudent(@PathVariable Long id) {
        service.deleteStudent(id);
    }

    @GetMapping("/list")
    public List<Student> getStudent() {
        return service.getStudentList();
    }
}
