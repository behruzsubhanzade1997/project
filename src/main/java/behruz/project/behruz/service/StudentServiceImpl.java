package behruz.project.behruz.service;

import behruz.project.behruz.controller.dto.StudentDto;
import behruz.project.behruz.controller.dto.StudentRequestDto;
import behruz.project.behruz.domain.Student;
import behruz.project.behruz.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("service1")
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {


    private final ModelMapper modelMapper;
    private final StudentRepository studentRepository;

    @Override
    public Student getById(Long id) {
        return studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));

    }

    @Override
    public StudentDto createStudent(StudentRequestDto studentDto) {
        Student student = modelMapper.map(studentDto, Student.class);
        return modelMapper.map(studentRepository.save(student),StudentDto.class);
    }

    @Override
    public Student updateStudent(Long id, Student student) {
        checkIfExists(id);
        student.setId(id);
        return studentRepository.save(student);
    }

    @Override
    public void deleteStudent(Long id) {
        checkIfExists(id);
        studentRepository.deleteById(id);


    }

    @Override
    public List<Student> getStudentList() {
        return studentRepository.findAll();
    }

    private void checkIfExists(Long id) {
        studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));
    }
}
