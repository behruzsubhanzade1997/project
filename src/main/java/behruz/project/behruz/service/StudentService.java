package behruz.project.behruz.service;

import behruz.project.behruz.controller.dto.StudentDto;
import behruz.project.behruz.controller.dto.StudentRequestDto;
import behruz.project.behruz.domain.Student;

import java.util.List;

public interface StudentService {
    Student getById(Long id);
    StudentDto createStudent(StudentRequestDto student);
    Student updateStudent(Long id, Student student);
    void deleteStudent(Long id);
    List<Student> getStudentList();
}
