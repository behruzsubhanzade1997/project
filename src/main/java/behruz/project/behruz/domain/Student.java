package behruz.project.behruz.domain;

import lombok.Data;

import javax.persistence.*;


@Entity
@Table(name ="students")
@Data
public class Student {

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;

    private String lastName;
}
