package behruz.project.behruz.repository;


import behruz.project.behruz.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
