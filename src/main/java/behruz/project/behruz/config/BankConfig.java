package behruz.project.behruz.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
public class BankConfig {
//    @Value("${bank.name}")
    private String bankName;
//
//    @Value("${bank.opened}")
    private Long bankOpened;
//
//    @Value("${bank.offices}")
    private List<String> offices;
}
